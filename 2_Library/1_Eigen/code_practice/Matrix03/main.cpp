#include <iostream>
#include <cmath>
using namespace std;

#include <Eigen/Core>
#include <Eigen/Geometry>

int main(int argc, char **argv) {
    
    Eigen::AngleAxisd t_v(M_PI/4,Eigen::Vector3d(0,0,1));
    Eigen::Matrix3d t_R = t_v.matrix();
    Eigen::Quaterniond t_Q(t_v);
    
    cout<<"t_R = \n"<<t_R.transpose()<<endl;
    //cout<<"t_Q = \n"<<t_Q<<endl;
    
    Eigen::AngleAxisd V2;
    V2.fromRotationMatrix(t_R);
    
    Eigen::AngleAxisd V3;
    V3 = t_R;
    cout<<"V3\n"<<V3.matrix()<<endl;
    
    Eigen::AngleAxisd V4(t_R);
    cout<<"V4\n"<<V4.matrix()<<endl;
    
    Eigen::AngleAxisd V5(t_Q);
    cout<<"V5\n"<<V5.matrix()<<endl;
    
    Eigen::Quaterniond Q1(cos((M_PI/4)/2),0*sin((M_PI/4)/2),0*sin((M_PI/4)/2),1*sin((M_PI/4)/2));
    cout<< "Quaterniond\n"<<Q1.coeffs()<<endl;
    cout<< "Quaterniond\n"<<Q1.matrix()<<endl;
    
    cout<<Q1.x()<<endl;
    cout<<Q1.y()<<endl;
    cout<<Q1.z()<<endl;
    cout<<Q1.w()<<endl;
    
    Eigen::Matrix3d R1 = Eigen::Matrix3d::Identity();
    cout<<"Rotation_matrix"<<endl<<R1;
    
    cout << "Hello, world!" << std::endl;
    return 0;
}
