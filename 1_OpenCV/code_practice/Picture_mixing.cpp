#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc, char **argv) {

	Mat src1, src2, dst;
	src1 = imread("F:/opencv/Luffy.png");
	src2 = imread("F:/opencv/myheart.png");
	if (!src1.data) {
		cout<<"could not open image1";
		return -1;
	}
	if (!src2.data) {
		cout << "could not open image2";
		return -1;
	}
	double alpha = 0.5;
	if (src1.rows == src2.rows&&src1.cols == src2.cols &&src1.type() == src2.type()) {
		imshow("1", src1);
		imshow("2", src2);
		
		addWeighted(src1, alpha, src2, (1.0 - alpha), 0, dst);
		//multiply(src1,src2,dst,1.0);
		namedWindow("blend demo", CV_WINDOW_AUTOSIZE);
		imshow("blend demo", dst);
	}
	else {
		printf("could not blend images,the size of image is not same");
		return -1;
	}

	waitKey(0);

	return 0;
}