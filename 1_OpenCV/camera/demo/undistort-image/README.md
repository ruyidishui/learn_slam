# 图像去畸变

## 原理分析

​	在各种涉及到几何关系的视觉算法中，我们常常需要对图像进行去畸变操作，因为畸变后的图像原有的一些几何特征都会变形，例如直线不再是直线等等，因此需要进行去畸变操作.

​	由畸变方程:

![image-20200506155800520](./figures/image-20200506155800520.png)

我们可以根据畸变前的坐标得到畸变后的点坐标，但是反向的操作较为困难，如果对单个点计算可能计算量还可以承受，若是对整张图去畸变，那么显然计算效率就比较低了。

​	通常对图像去畸变的操作一般是先构建一张空图像，对于图像中每个像素，利用畸变关系计算出畸变后图像的坐标，由于我们当前已经拥有了畸变后的图像，于是直接根据坐标去获取值就可以了，这样就可以建立像素坐标之间的一一对应关系，便可以得到去畸变后的图像。



## 代码解读

由于计算出来的畸变后坐标通常不是整数，所以需要插值处理

```c++
uint8_t interpolate_bilinear(const cv::Mat& img,const cv::Point2d& point)
{
    int i =  std::floor(std::max(point.x,0.0));
    int j =  std::floor(std::max(point.y,0.0));
    uint8_t  v00 = img.at<uint8_t>(j,i);
    uint8_t  v01 = img.at<uint8_t>(j+1,i);
    uint8_t  v10 = img.at<uint8_t>(j,i+1);
    uint8_t  v11 = img.at<uint8_t>(j+1,i+1);

    double u = point.x - i;
    double v = point.y - j;

    double value  = (1-u)*(1-v)*v00 + (1-u)*v*v01 +  u*(1-v)*v10 + u*v*v11;
    return (uint8_t)value;
}
```
在对点进行畸变操作时，通常要将其变换回归一化坐标系，因为畸变的作用是在归一化坐标系上进行的
```c++
cv::Mat undistort_image(const cv::Mat& origin)
{
    cv::Mat result(origin.rows,origin.cols,origin.type());
    for(int j=0;j < result.rows ; j++)
        for(int i=0; i < result.cols ; i++)
        {
            double n_x = (i - cx)/fx;
            double n_y = (j - cy)/fy;
            cv::Point2d src(n_x,n_y),dst;
            distort_point(src,&dst);
            dst.x = dst.x*fx + cx;
            dst.y = dst.y*fy + cy;
            if(std::ceil(dst.x) > origin.cols || std::ceil(dst.y) > origin.rows )
            {
                result.at<uint8_t>(j,i) = 0;
            }
            else{
                result.at<uint8_t>(j,i) = interpolate_bilinear(origin,dst);
            }
        }
    return result;
}

```


## 结果

![image-20200506162127026](figures/image-20200506162127026.png)

可以看到在去畸变后，图像中的直线恢复成了直线，同时由于裁剪的影响，图像显示的范围也变小了.